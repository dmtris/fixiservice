const images = {

    //splash screen
    logo: require('../resources/images/splash/logo.png'),
    splash_bkg: require('../resources/images/splash/1-splash.png'),
    
    //auth screen
    login_but: require('../resources/images/login/login.png'),
    signup_but: require('../resources/images/login/signup.png'),
    resetpass_but: require('../resources/images/login/resetpassword.png'),
    backlogin_but: require('../resources/images/login/backlogin.png'),

    //home screen
    diag_btn: require('../resources/images/home/diag_btn.png'),
    warranty_btn: require('../resources/images/home/subwarranty_btn.png'),
    document_btn: require('../resources/images/home/submit_btn.png'),

    //products
    aircon_btn: require('../resources/images/home/aircon_btn.png'),
    heatpump_btn: require('../resources/images/home/heatbump_btn.png'),
    thermostat_btn: require('../resources/images/home/thermostat_btn.png'),
    furance_btn: require('../resources/images/home/furance_btn.png'),
    boiler_btn: require('../resources/images/home/boiler_btn.png'),
    evaporator_btn: require('../resources/images/home/evaporator_btn.png'),

    //guide
    acon_icon: require('../resources/images/home/acon_icon.png'),
    furance_icon: require('../resources/images/home/furance_icon.png'),
    heatpump_icon: require('../resources/images/home/heatpump_icon.png'),
    boiler_icon: require('../resources/images/home/boiler_icon.png'),
    thermost_icon: require('../resources/images/home/thermost_icon.png'),
    evaporate_icon: require('../resources/images/home/evapor_icon.png'),
    chat_btn: require('../resources/images/home/chat_btn.png'),
    assist_btn: require('../resources/images/home/assist_btn.png'),

    accept_call: require('../resources/images/home/accept_call.png'),
    cancel_call: require('../resources/images/home/cancel_call.png'),

    claim_pump: require('../resources/images/home/claim_pump.png'),
    add_btn:require('../resources/images/home/add_btn.png'),
    smt_btn:require('../resources/images/home/smt_btn.png'),

    back_btn:require('../resources/images/home/back_btn.png'),
};

export default images;