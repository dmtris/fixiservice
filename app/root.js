import React, { Component } from 'react';
import { Animated, Easing } from 'react-native';
import { connect } from 'react-redux';
import LoginScreen from './screens/auth/login.js';
import SignupScreen from './screens/auth/signup.js'
import SplashScreen from './screens/auth/splashScreen.js';
import HomeScreen from './screens/home/home.js';
import AssistScreen from './screens/home/assist.js'
import GuideScreen from './screens/home/guide.js'
import CallScreen from './screens/home/call.js'
import ClaimScreen from './screens/home/claim.js'
import ChatBotScreen from './screens/home/chatbotscreen.js'
import { StackNavigator } from 'react-navigation';

const fade = (props) => {
    const { position, scene } = props
  
    const index = scene.index
  
    const translateX = 0
    const translateY = 0
  
    const opacity = position.interpolate({
      inputRange: [index - 0.7, index, index + 0.7],
      outputRange: [0.3, 1, 0.3]
    })
  
    return {
      opacity,
      transform: [{ translateX }, { translateY }]
    }
}

const AppStack = StackNavigator({
    
    Login: {
        screen: LoginScreen
    },
    Signup: {
        screen: SignupScreen
    },
    Home: {
        screen: HomeScreen
    },

    Assist: {
        screen: AssistScreen
    },

    Claim: {
        screen: ClaimScreen
    },

    Guide: {
        screen: GuideScreen
    },

    Call: {
        screen: CallScreen
    },

    ChatBotScr: {
        screen: ChatBotScreen
    }
    
}, {
    headerMode: 'none',
});



const SplashStack = StackNavigator({
    Splash: {
        screen: SplashScreen
    },

    AppStack: {
        screen: AppStack
    }
}, {
    headerMode: 'none',
    transitionConfig: () => ({
        transitionSpec: {
          duration: 500,
          useNativeDriver: true,
          easing: Easing.out(Easing.poly(4)),
          timing: Animated.timing,
        },
        screenInterpolator: (props) => {
          return fade(props)
        }
      })
});

class Root extends Component{

    render(){
        // return <TabNav />
        return <SplashStack />
    }
}


const mapStateToProps = function (state) {
    return{
        isAuthenticated: true
    }
};

//make this component available to the app
export default connect(mapStateToProps)(Root);
