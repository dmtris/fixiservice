import React, { Component } from 'react';
import { View, Image, StyleSheet, Text, TextInput, Platform, TouchableOpacity } from 'react-native';
import {width,height} from 'react-native-dimension';
import {NavigationActions} from 'react-navigation';
import images from '../../const/images';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class SignUpScreen extends Component{
    constructor(props){
        super(props);
        this.state = {
            firstActive: false,
            lastActive: false,
            passwordActive: false,
            emailActive: false,
            phoneActive: false,
        };
    }

    navigateLogin = () => {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'Login'})
            ]
        });
        this.props.navigation.dispatch(resetAction);
    }

    render(){

        return(

            <View
                style={styles.inputContainer}>
                
                <View
                    style={styles.logoContainer}>
                    <Image 
                        source={images.logo}
                        style={styles.logo}
                        resizeMode='contain'
                    />

                </View>

                <KeyboardAwareScrollView  
                    resetScrollToCoords={{x : 0, y: 0}}
                    scrollEnabled={false}
                    behavior="padding" 
                    enabled>

                    <View style={[styles.inputSection,{borderColor: this.state.firstActive === true ? '#21406e' : '#cccccc'}]}>
                        <TextInput 
                            style={styles.inputBox}
                            underlineColorAndroid='rgba(0,0,0,0)' 
                            placeholder="Firstname"
                            placeholderTextColor={this.state.firstActive === true ? '#21406e' : '#999999'}
                            onFocus={() => this.setState({firstActive:true})}
                            onBlur={() => this.setState({firstActive:false})}
                            />
                    </View>

                    <View style={[styles.inputSection,{marginTop:height(1)}, {borderColor: this.state.lastActive === true ? '#21406e' : '#cccccc'}]}>
                        
                        <TextInput 
                            style={[styles.inputBox]}
                            underlineColorAndroid='rgba(0,0,0,0)' 
                            placeholder="Lastname"
                            placeholderTextColor={this.state.lastActive === true ? '#21406e' : '#999999'}
                            onFocus={() => this.setState({lastActive:true})}
                            onBlur={() => this.setState({lastActive:false})}
                            />
                    </View>

                    <View style={[styles.inputSection,{marginTop:height(1)}, {borderColor: this.state.emailActive === true ? '#21406e' : '#cccccc'}]}>

                        <TextInput 
                            style={[styles.inputBox]}
                            underlineColorAndroid='rgba(0,0,0,0)' 
                            placeholder="Email"
                            autoCapitalize='none'
                            placeholderTextColor={this.state.emailActive === true ? '#21406e' : '#999999'}
                            onFocus={() => this.setState({emailActive:true})}
                            onBlur={() => this.setState({emailActive:false})}
                            />
                    </View>

                    <View style={[styles.inputSection,{marginTop:height(1)}, {borderColor: this.state.phoneActive === true ? '#21406e' : '#cccccc'}]}>
                        
                        <TextInput 
                            style={[styles.inputBox]}
                            underlineColorAndroid='rgba(0,0,0,0)' 
                            placeholder="Phone"
                            autoCapitalize='none'
                            placeholderTextColor={this.state.phoneActive === true ? '#21406e' : '#999999'}
                            onFocus={() => this.setState({phoneActive:true})}
                            onBlur={() => this.setState({phoneActive:false})}
                            />
                    </View>

                    <View style={[styles.inputSection,{marginTop:height(1)}, {borderColor: this.state.passwordActive === true ? '#21406e' : '#cccccc'}]}>
                        
                        <TextInput 
                            style={[styles.inputBox]}
                            underlineColorAndroid='rgba(0,0,0,0)' 
                            placeholder="Password"
                            autoCapitalize='none'
                            secureTextEntry={true}
                            placeholderTextColor={this.state.passwordActive === true ? '#21406e' : '#999999'}
                            onFocus={() => this.setState({passwordActive:true})}
                            onBlur={() => this.setState({passwordActive:false})}
                            />
                    </View>


                    <TouchableOpacity>
                        <Image
                            source={images.signup_but}
                            resizeMode='contain'
                            style={styles.button}
                        />
                    </TouchableOpacity>
                    
                    <View style={{flexDirection: 'row', justifyContent:'center'}}>
                        <Text
                            style={[styles.normalText,{marginTop: height(2)}]}>
                            Already Have An Account? 
                        </Text>
                        <TouchableOpacity onPress={this.navigateLogin}>
                            <Text
                                style={[styles.normalText,{marginTop: height(2),fontWeight: 'bold',color:'#21406e'}]}>
                                &nbsp;LOGIN
                            </Text>
                        </TouchableOpacity>
                    </View>
        
                </KeyboardAwareScrollView>

                
            </View>
        )
    }
}
const styles = StyleSheet.create({
        
    inputContainer: {
        flex: 1,
        paddingTop: height(5),
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'green'
    },

    logoContainer: {
        // backgroundColor: 'green',
        flex: 0.4,
        alignItems: 'center',
        
         marginTop: height(3),
    },
    logo: {
        alignItems: 'center',
        justifyContent: 'center',
        width: width(100),
        height: height(20),
    },

    inputSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        // borderColor: 'rgba(0,255,0,1)',
        borderColor: '#cccccc',
        backgroundColor: '#fbfbfb',
    },

    userIcon: {
        padding: 10,
    },

    inputBox: {
        flex: 1,
        width: width(85),
        ...Platform.select({
            ios: {
              height: height(7),
              fontSize: 14,
            },
            android: {
                fontSize: 14,
            },
        }),
        // backgroundColor: 'rgba(255,0,0,1)',
        
        paddingHorizontal: 16,
        color: '#999999',
        // marginVertical: height(1.5),
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
    
    },

    button: {
        width: width(85),
        height: height(10),
        // ...Platform.select({
        //     ios: {
        //       marginTop: height(5),
        //     },
        //     android: {
        //         marginTop: height(2.5),
        //     },
        // }),
        marginTop: height(4),
    },

    titleText: {
        // marginVertical: height(2),
        ...Platform.select({
            ios: {
              fontSize: 17,
              marginVertical: height(5),
            },
            android: {
                fontSize: 20,
                marginBottom: height(5),
            },
        }),
        // color: 'rgba(0,0,0,1)',
        
        color: '#00bcd4',
        alignSelf: 'center',
        fontWeight: 'bold',
    },

    normalText: {
        // marginVertical: height(2),
        ...Platform.select({
            ios: {
              fontSize: 12,
            },
            android: {
                fontSize: 14,
            },
        }),
        // color: 'rgba(0,0,0,1)',
        color: '#999999',
        alignSelf: 'center',
    }
});
export default SignUpScreen;