import React, { Component } from 'react';
import { View,ImageBackground, Image, StyleSheet, Text, TouchableOpacity } from 'react-native';
import {width,height} from 'react-native-dimension';
import images from '../../const/images';

class SplashScreen extends Component{
    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.navigate('AppStack');
        }, 2000);
    }

    render(){
        return(
            <ImageBackground
                style={styles.container}
                source = {images.splash_bkg}
                resizeMode='cover'>
                
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        flexDirection: 'column',
        
    },
    
});
export default SplashScreen;