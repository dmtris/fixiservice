import React, { Component } from 'react';
import { View, Image, StyleSheet, Text, Platform, TouchableOpacity } from 'react-native';
import {width,height} from 'react-native-dimension';
import images from '../../const/images';
import { Col, Row, Grid } from "react-native-easy-grid";

class AssistScreen extends Component{
    
    guideAirCon = () => {
        this.props.navigation.navigate('Guide', {
        itemId: 1,
        otherParam: 'Air Conditioner',
        image: images.acon_icon,
      });
    }

    guideFurance = () => {
        this.props.navigation.navigate('Guide', {
        itemId: 2,
        otherParam: 'Furance',
        image: images.furance_icon,
      });
    }

    guideHeatPump = () => {
        this.props.navigation.navigate('Guide', {
        itemId: 3,
        otherParam: 'Heat Pump',
        image: images.heatpump_icon,
      });
    }

    guideBoiler = () => {
        this.props.navigation.navigate('Guide', {
        itemId: 4,
        otherParam: 'Boiler',
        image: images.boiler_icon,
      });
    }

    guideThermostat = () => {
        this.props.navigation.navigate('Guide', {
        itemId: 5,
        otherParam: 'Thermostat',
        image: images.thermost_icon,
      });
    }

    guideEvaporate = () => {
        this.props.navigation.navigate('Guide', {
        itemId: 6,
        otherParam: 'Evaporate Coil',
        image: images.evaporate_icon,
      });
    }

    render(){
        return(
            <View 
                style={styles.container}>
                 <View style = {styles.header}>
                    <TouchableOpacity style = {{paddingLeft:width(2.5)}}
                        onPress= {() => this.props.navigation.goBack(null)}>
                        <Image
                            source={images.back_btn}
                            resizeMode='contain'
                            style = {styles.backButton}
                        />
                    </TouchableOpacity>
                   
                    <Text style = {styles.headerFont}> 
                        Diagnotic Assistant
                    </Text>

                </View>
                <View style = {styles.title}>
                    <Text style = {{fontSize:20, color:'black'}}>
                        Why do you need help diagnosing?
                    </Text>
                </View>
                <Grid>
                    <Col>
                        <Row style = {styles.rowStyle}>
                            <TouchableOpacity onPress={this.guideAirCon}>
                                <Image
                                    source={images.aircon_btn}
                                    resizeMode='contain'
                                    style={styles.button}
                                />
                            </TouchableOpacity>
                        </Row> 
                        <Row style = {styles.rowStyle}>
                            <TouchableOpacity onPress={this.guideHeatPump}>
                                <Image
                                    source={images.heatpump_btn}
                                    resizeMode='contain'
                                    style={styles.button}
                                />
                            </TouchableOpacity>
                        </Row> 

                         <Row style = {styles.rowStyle}>
                            <TouchableOpacity onPress={this.guideThermostat}>
                                <Image
                                    source={images.thermostat_btn}
                                    resizeMode='contain'
                                    style={styles.button}
                                />
                            </TouchableOpacity>
                        </Row> 
                    </Col>

                    <Col>
                        <Row style = {styles.rowStyle}>
                            <TouchableOpacity onPress={this.guideFurance}>
                                <Image
                                    source={images.furance_btn}
                                    resizeMode='contain'
                                    style={styles.button}
                                />
                            </TouchableOpacity>
                        </Row> 
                        <Row style = {styles.rowStyle}>
                            <TouchableOpacity onPress={this.guideBoiler}>
                                <Image
                                    source={images.boiler_btn}
                                    resizeMode='contain'
                                    style={styles.button}
                                />
                            </TouchableOpacity>
                        </Row> 
                        <Row style = {styles.rowStyle}>
                            <TouchableOpacity onPress={this.guideEvaporate}>
                                <Image
                                    source={images.evaporator_btn}
                                    resizeMode='contain'
                                    style={styles.button}
                                />
                            </TouchableOpacity>
                        </Row> 
                    </Col>
                </Grid>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
    },


    headerFont: {
        paddingLeft: width(22.5),
        color: 'white',
        fontSize: 20,
    },

    backButton: {
        // backgroundColor: 'red',
        width: height(3),
    },

    header:{
        ...Platform.select({
            ios: {
              paddingTop: 24,
              height:height(9.5),
            },
            android: {
                height:height(7.5),
            }
        }),
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#21406e',
        width:width(100),
    },

    title: {
        marginTop: height(3),
        marginLeft: width(3),
        marginBottom: height(3),
    },
    
    rowStyle:{
        // backgroundColor:'grey',
        justifyContent: 'center',
        alignItems: 'center',
    },

    button: {
        width:width(40),
        justifyContent: 'center',
        alignItems: 'center',
    }
    
});
export default AssistScreen;