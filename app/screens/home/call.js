import React, { Component } from 'react';
import { View, Image, StyleSheet, Text, Platform, TouchableOpacity } from 'react-native';
import {width,height} from 'react-native-dimension';
import images from '../../const/images';

class CallScreen extends Component{
    
    render(){
        return(
            <View 
                style={styles.container}>
                 
                <View 
                    style = {styles.logoContainer}>
                    <Image 
                        source={images.logo}
                        style={styles.logo}
                        resizeMode='contain'
                    />
                </View>

                <View
                    style = {styles.buttonContainer}>

                    <View>
                        <TouchableOpacity 
                            onPress= {() => this.props.navigation.goBack(null)}>
                                <Image
                                    source={images.cancel_call}
                                    resizeMode='contain'
                                    style={styles.button}
                                />
                        </TouchableOpacity>

                    </View>

                    <View style = {{marginLeft:width(20)}}>
                        <TouchableOpacity>
                            <Image
                                source={images.accept_call}
                                resizeMode='contain'
                                style={styles.button}
                            />
                        </TouchableOpacity>
                    </View>                    
                    
                </View>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        
    },

    logoContainer: {
        flex : 3,
        alignItems: 'center',
        justifyContent: 'center',
    },

    buttonContainer: {
        flex : 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        // backgroundColor:'green',
    },

    button: {
        width:width(25),
    }
});
export default CallScreen;