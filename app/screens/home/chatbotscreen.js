import React, { Component } from 'react';
import { View, Image, StyleSheet, Text, Platform, TouchableOpacity } from 'react-native';
import {width,height} from 'react-native-dimension';
import images from '../../const/images';
import ChatBot from 'react-native-chatbot';

class ChatBotScreen extends Component{
    call = () => {
        this.props.navigation.navigate('Call');
    }
    
    render(){
        const steps = [
            [
                {
                    id: '0',
                    message: 'NO COOLING OR INSUFFICIENT COOLING',
                    trigger: '0_0',
                },
                {
                    id: '-1',
                    message: 'OK. Thanks. Have a nice day',
                    trigger: '0',                    
                },
                {
                    id: '0_0',
                    options: [
                        { value: '0_1', label: 'COMPRESS WILL NOT RUN', trigger: '0_1' },
                        { value: '0_2', label: 'COMPRESSOR RUNS BUT CYCLES ON INTERNAL OVERLOAD', trigger: '0_2' },
                        { value: '0_3', label: 'COMPRESSOR RUNS BUT INSUFFICIENT COOLING', trigger: '0_3' },
                    ],
                },
                {
                    id: '0_1',
                    options: [
                        { value: '0_1_1', label: 'CONTACTOR OPEN', trigger: '0_1_1' },
                        { value: '0_1_2', label: 'CONTACTOR CLOSED', trigger: '0_1_2' },
                    ],
                },
                {
                    id: '0_1_1',
                    options: [
                        { value: '0_1_1_1', label: 'POWER SUPPLY', trigger: '-1'},
                        { value: '0_1_1_2', label: 'DETECTIVE LOW-VOLTAGE TRANSFORMER', trigger: '-1'},
                        { value: '0_1_1_3', label: 'OPEN THERMOSTAT', trigger: '-1'},
                        { value: '0_1_1_4', label: 'OPEN CONTROL CIRCUIT', trigger: '-1'},
                        { value: '0_1_1_5', label: 'LOSS OF CHARGE', trigger: '-1'},
                        { value: '0_1_1_6', label: 'CONTACTOR OR COIL DEFECTIVE', trigger: '-1'},
                        { value: '0_1_1_7', label: 'LOOSE ELECTRICAL CONNECTION', trigger: '-1'},
                    ],
                },
                {
                    id: '0_1_2',
                    options: [
                        { value: '0_1_2_1', label: 'COMPRESSOR POWER SUPPLY OPEN', trigger: '-1'},
                        { value: '0_1_2_2', label: 'LOOSE LEADS AT COMPRESSOR', trigger: '-1'},
                        { value: '0_1_2_3', label: 'FAULTY START GEAR (1-PH)', trigger: '-1'},
                        { value: '0_1_2_4', label: 'OPEN SHORTED OR GROUNDED COMPRESSOR MOTOR WINDINGS', trigger: '-1'},
                        { value: '0_1_2_5', label: 'COMPRESSOR STUCK', trigger: '-1'},
                        { value: '0_1_2_6', label: 'COMPRESSOR INTERNAL PROTECTION OPEN', trigger: '-1'},
                        { value: '0_1_2_7', label: 'DEFECTIVE RUN CAPACITOR', trigger: '-1'},
                        { value: '0_1_2_8', label: 'DEFECTIVE START CAPACITOR', trigger: '-1'},
                    ],
                },
                {
                    id: '0_2',
                    options: [
                        { value: '0_2_1', label: 'OUTDOOR FAN STOPPED OR CYCLING ON OVERLOAD', trigger: '0_2_1' },
                        { value: '0_2_2', label: 'OUTDOOR AIR RESTRICTED OR RECIRCULATING', trigger: '-1' },
                        { value: '0_2_3', label: 'RESTRICTED DISCHARGE TUBE', trigger: '-1' },
                        { value: '0_2_4', label: 'OVERCHARGE OR NON-CONDENSABLES IN SYSTEM', trigger: '-1' },
                        { value: '0_2_5', label: 'LOW REFRIGERANT CHARGE', trigger: '-1' },
                        { value: '0_2_6', label: 'LINE VOLTAGE TOO HIGH OR LOW', trigger: '-1' },
                        { value: '0_2_7', label: 'DEFECTIVE RUN CAPACITOR', trigger: '-1' },
                        { value: '0_2_8', label: 'COMPRESSOR BEARINGS', trigger: '-1' },
                        { value: '0_2_9', label: 'HIGH SUPERHEAT', trigger: '-1' },
                        { value: '0_2_10', label: 'DEFECTIVE START CAPACITOR', trigger: '-1' },
                    ],
                },
                {
                    id: '0_2_1',
                    options: [
                        { value: '0_2_1_1', label: 'LOOSE LEAD AT FAN MOTOR', trigger: '-1' },
                        { value: '0_2_1_2', label: 'MOTOR DEFECTIVE', trigger: '-1' },
                        { value: '0_2_1_3', label: 'INCORRECT OFM CAPACITOR', trigger: '-1' },
                    ],
                },
                {
                    id: '0_3',
                    options: [
                        { value: '0_3_1', label: 'LOW SUCTION PRESSURE', trigger: '0_3_1' },
                        { value: '0_3_2', label: 'HIGH SUCTION LOW HEAD PRESSURE', trigger: '0_3_2' },
                        { value: '0_3_3', label: 'HIGH SUCTION LOW SUPERHEAT', trigger: '0_3_3' },
                    ],
                },
                {
                    id: '0_3_1',
                    options: [
                        { value: '0_3_1_1', label: 'DIRTY AIR FILTERS', trigger: '-1' },
                        { value: '0_3_1_2', label: 'DUCT RESTRICTED', trigger: '-1' },
                        { value: '0_3_1_3', label: 'DAMPERS PARTLY CLOSED', trigger: '-1' },
                        { value: '0_3_1_4', label: 'INDOOR COIL FROSTED', trigger: '-1' },
                        { value: '0_3_1_5', label: 'SLIGHTLY LOW ON REFRIGERANT', trigger: '-1' },
                        { value: '0_3_1_6', label: 'LIQUID LINE SLIGHTLY RESTRICTED', trigger: '-1' },
                        { value: '0_3_1_7', label: 'PISTON RESTRICTED', trigger: '-1' },
                        { value: '0_3_1_8', label: 'INCORRECT SIZE PISTON', trigger: '-1' },
                        { value: '0_3_1_9', label: 'INDOOR COIL STRAINER RESTRICTED', trigger: '-1' },
                        { value: '0_3_1_10', label: 'INDOOR BLOWER MOTOR DEFECTIVE OR CYCLING ON OL', trigger: '-1' },
                    ],
                },
                {
                    id: '0_3_2',
                    options: [
                        { value: '0_3_2_1', label: 'DEFECTIVE COMPRESSOR VALVES', trigger: '-1' },
                        { value: '0_3_2_2', label: 'INTERNAL PRESSURE RELIEF OPEN', trigger: '-1' },
                    ],
                },
                {
                    id: '0_3_3',
                    options: [
                        { value: '0_3_3_1', label: 'UNIT OVERCHARGED', trigger: '-1' },
                        { value: '0_3_3_2', label: 'INCORRECT SIZE PISTON', trigger: '-1' },
                        { value: '0_3_3_3', label: 'FAILED TXV', trigger: '-1' },
                    ],
                },
            ],
            [
                {
                    id: '0',
                    message: 'NO HEATING OR INSUFFICIENT HEATING',
                    trigger: '0_0',
                },
                {
                    id: '-1',
                    message: 'OK. Thanks. Have a nice day',
                    trigger: '0',                    
                },
                {
                    id: '0_0',
                    options: [
                        { value: '0_1', label: 'COMPRESS WILL NOT RUN', trigger: '0_1' },
                        { value: '0_2', label: 'COMPRESSOR RUNS BUT CYCLES ON INTERNAL OVERLOAD', trigger: '0_2' },
                        { value: '0_3', label: 'COMPRESSOR RUNS BUT INSUFFICIENT HEATING', trigger: '0_3' },
                    ],
                },
                {
                    id: '0_1',
                    options: [
                        { value: '0_1_1', label: 'CONTACTOR OPEN', trigger: '0_1_1' },
                        { value: '0_1_2', label: 'CONTACTOR CLOSED', trigger: '0_1_2' },
                    ],
                },
                {
                    id: '0_1_1',
                    options: [
                        { value: '0_1_1_1', label: 'DETECTIVE LOW-VOLTAGE TRANSFORMER', trigger: '-1'},
                        { value: '0_1_1_2', label: 'REMOTE CONTROL CENTER DEFECTIVE', trigger: '-1'},
                        { value: '0_1_1_3', label: 'CONTACTOR COIL OPEN OR SHORTED', trigger: '-1'},
                        { value: '0_1_1_4', label: 'OPEN INDOOR THERMOSTAT', trigger: '-1'},
                        { value: '0_1_1_5', label: 'LIQUID-LINE PRESSURE SWITCH OPEN', trigger: '-1'},
                        { value: '0_1_1_6', label: 'LOSS OF CHARGE', trigger: '-1'},
                        { value: '0_1_1_7', label: 'OPEN CONTROL CIRCUIT', trigger: '-1'},
                    ],
                },
                {
                    id: '0_1_2',
                    options: [
                        { value: '0_1_2_1', label: 'COMPRESSOR POWER SUPPLY', trigger: '-1'},
                        { value: '0_1_2_2', label: 'LOOSE LEADS AT COMPRESSOR', trigger: '-1'},
                        { value: '0_1_2_3', label: 'FAULTY START GEAR (1-PH)', trigger: '-1'},
                        { value: '0_1_2_5', label: 'COMPRESSOR STUCK', trigger: '-1'},
                        { value: '0_1_2_4', label: 'COMPRESSOR INTERNAL OVERLOAD OPEN', trigger: '-1'},
                        { value: '0_1_2_6', label: 'OPEN SHORTED OR GROUNDED COMPRESSOR WINDINGS', trigger: '-1'},
                        { value: '0_1_2_7', label: 'DEFECTIVE RUN CAPACITOR', trigger: '-1'},
                        { value: '0_1_2_8', label: 'DEFECTIVE START CAPACITOR', trigger: '-1'},
                    ],
                },
                {
                    id: '0_2',
                    options: [
                        { value: '0_2_1', label: 'DIRTY FILTERS OR INDOOR COIL', trigger: '-1' },
                        { value: '0_2_2', label: 'INDOOR FAN STOPPED OR CYCLING ON OVERLOAD', trigger: '0_2_2' },
                        { value: '0_2_3', label: 'DAMAGED REVERSING VALVE', trigger: '-1' },
                        { value: '0_2_4', label: 'RESTRICTION IN DISCHARGE LINE', trigger: '-1' },
                        { value: '0_2_5', label: 'OVERCHARGE OR NON-CONDENSABLES IN SYSTEM', trigger: '-1' },
                        { value: '0_2_6', label: 'LOW REFRIGERANT CHARGE', trigger: '-1' },
                        { value: '0_2_7', label: 'LINE VOLTAGE TOO HIGH OR LOW', trigger: '-1' },
                        { value: '0_2_8', label: 'DEFECTIVE RUN CAPACITOR (1-PH)', trigger: '-1' },
                        { value: '0_2_9', label: 'COMPRESSOR BEARINGS', trigger: '-1' },
                        { value: '0_2_10', label: 'HIGH-LOAD CONDITION', trigger: '-1' },
                        { value: '0_2_11', label: 'REVERSING VALVE JAMMED IN MIDPOSITION', trigger: '-1' },
                        { value: '0_2_12', label: 'HIGH SUPERHEAT', trigger: '-1' },
                        { value: '0_2_13', label: 'DEFECTIVE START CAPACITOR', trigger: '-1' },
                    ],
                },
                {
                    id: '0_2_2',
                    options: [
                        { value: '0_2_2_1', label: 'DEFECTIVE FAN MOTOR CAPACITOR', trigger: '-1' },
                        { value: '0_2_2_2', label: 'LOOSE LEAD AT FAN MOTOR', trigger: '-1' },
                        { value: '0_2_2_3', label: 'FAN MOTOR BURNED OOUT', trigger: '-1' },
                    ],
                },
                {
                    id: '0_3',
                    options: [
                        { value: '0_3_1', label: 'LOW SUCTION LOW HEAD', trigger: '0_3_1' },
                        { value: '0_3_2', label: 'STRIP HEATERS NOT OPERATING', trigger: '0_3_2' },
                    ],
                },
                {
                    id: '0_3_1',
                    options: [
                        { value: '0_3_1_1', label: 'OUTDOOR FAN STOPPED', trigger: '0_3_1_1' },
                        { value: '0_3_1_2', label: 'OUTDOOR FAN RUNNING', trigger: '0_3_1_2' },
                    ],
                },
                {
                    id: '0_3_1_1',
                    options: [
                        { value: '0_3_1_1_1', label: 'LOOSE LEADS AT OUTDOOR FAN MOTOR', trigger: '-1' },
                        { value: '0_3_1_1_2', label: 'INTERNAL FAN MOTOR KLIXON OPEN', trigger: '-1' },
                        { value: '0_3_1_1_3', label: 'FAN MOTOR BURNED OUT', trigger: '-1' },
                        { value: '0_3_1_1_4', label: 'DEFROST RELAY N.C. CONTACTS OPEN ON CIRCUIT BOARD', trigger: '-1' },
                    ],
                },
                {
                    id: '0_3_1_2',
                    options: [
                        { value: '0_3_1_2_1', label: 'REVERSING VALVE STUCK', trigger: '-1' },
                        { value: '0_3_1_2_2', label: 'RESTRICTED LIQUID LINE', trigger: '-1' },
                        { value: '0_3_1_2_3', label: 'PISTON RESTRICTED OR IS CLOGGED', trigger: '-1' },
                        { value: '0_3_1_2_4', label: 'UNDER-CHARGED', trigger: '-1' },
                        { value: '0_3_1_2_5', label: 'OUTDOOR COIL DIRTY', trigger: '-1' },
                        { value: '0_3_1_2_6', label: 'STRAINER RESTRICTED', trigger: '-1' },
                        { value: '0_3_1_2_7', label: 'OUTDOOR COIL HEAVILY FROSTED', trigger: '0_3_1_2_7' },
                    ],
                },
                {
                    id: '0_3_1_2_7',
                    options: [
                        { value: '0_3_1_2_7_1', label: 'FAN MOTOR CONTACTS WELDED CLOSED IN DEFROST RELAY', trigger: '-1' },
                        { value: '0_3_1_2_7_2', label: 'REVERSING VALVE DID NOT SHIFT', trigger: '-1' },
                        { value: '0_3_1_2_7_3', label: 'UNIT NOT PROPERLY CHARGED', trigger: '-1' },
                        { value: '0_3_1_2_7_4', label: 'DEFECTIVE DEFROST THERMOSTAT', trigger: '-1' },
                        { value: '0_3_1_2_7_5', label: 'DEFROST THERMOSTAT IN POOR PHYSICAL CONTACT WITH TUBE', trigger: '-1' },
                        { value: '0_3_1_2_7_6', label: 'DEFECTIVE CIRCUIT BOARD', trigger: '-1' },
                        { value: '0_3_1_2_7_7', label: 'BAD ELECTRICAL CONNECTION ANYWHERE IN DEFROST CIRCUIT', trigger: '-1' },
                    ],
                },
                {
                    id: '0_3_2',
                    options: [
                        { value: '0_3_2_1', label: 'OUTDOOR THERMOSTAT DEFECTIVE', trigger: '0_3_2_1' },
                        { value: '0_3_2_2', label: 'STRIP HEATER RELAY OR CONTACTOR DEFECTIVE', trigger: '-1' },
                        { value: '0_3_2_3', label: 'OPENING IN POWER CIRCUIT TO HEATER ELEMENTS', trigger: '0_3_2_3' },
                        { value: '0_3_2_4', label: 'OPEN (KLIXON) OVER TEMPERATURE THERMOSTAT', trigger: '-1' },
                        { value: '0_3_2_5', label: 'DEFECTIVE ROOM THERMOSTAT (2ND STAGE)', trigger: '-1' },
                    ],
                },
                {
                    id: '0_3_2_1',
                    options: [
                        { value: '0_3_2_1_1', label: 'ODT SETTING TOO LOW', trigger: '-1' },
                        { value: '0_3_2_1_2', label: 'CAP TUBE PINCHED OR BULB NOT SENSING TRUE ODT', trigger: '-1' },
                    ],
                },
                {
                    id: '0_3_2_3',
                    options: [
                        { value: '0_3_2_3_1', label: 'BROKEN FUSE LINK', trigger: '-1' },
                        { value: '0_3_2_3_2', label: 'BROKEN HEATER ELEMENT', trigger: '-1' },
                    ],
                },
            ],
            [
                {
                    id: '0',
                    message: 'NO COOLING OR INSUFFICIENT COOLING',
                    trigger: '0_0',
                },
                {
                    id: '-1',
                    message: 'OK. Thanks. Have a nice day',
                    trigger: '0',                    
                },
                {
                    id: '0_0',
                    options: [
                        { value: '0_1', label: 'COMPRESS WILL NOT RUN', trigger: '0_1' },
                        { value: '0_2', label: 'COMPRESSOR RUNS BUT CYCLES ON INTERNAL OVERLOAD', trigger: '0_2' },
                        { value: '0_3', label: 'COMPRESSOR RUNS BUT INSUFFICIENT COOLING', trigger: '0_3' },
                    ],
                },
                {
                    id: '0_1',
                    options: [
                        { value: '0_1_1', label: 'CONTACTOR OPEN', trigger: '0_1_1' },
                        { value: '0_1_2', label: 'CONTACTOR CLOSED', trigger: '0_1_2' },
                    ],
                },
                {
                    id: '0_1_1',
                    options: [
                        { value: '0_1_1_1', label: 'POWER SUPPLY', trigger: '-1'},
                        { value: '0_1_1_2', label: 'DETECTIVE LOW-VOLTAGE TRANSFORMER', trigger: '-1'},
                        { value: '0_1_1_3', label: 'OPEN THERMOSTAT', trigger: '-1'},
                        { value: '0_1_1_4', label: 'OPEN CONTROL CIRCUIT', trigger: '-1'},
                        { value: '0_1_1_5', label: 'LOSS OF CHARGE', trigger: '-1'},
                        { value: '0_1_1_6', label: 'CONTACTOR OR COIL DEFECTIVE', trigger: '-1'},
                        { value: '0_1_1_7', label: 'LOOSE ELECTRICAL CONNECTION', trigger: '-1'},
                    ],
                },
                {
                    id: '0_1_2',
                    options: [
                        { value: '0_1_2_1', label: 'COMPRESSOR POWER SUPPLY OPEN', trigger: '-1'},
                        { value: '0_1_2_2', label: 'LOOSE LEADS AT COMPRESSOR', trigger: '-1'},
                        { value: '0_1_2_3', label: 'FAULTY START GEAR (1-PH)', trigger: '-1'},
                        { value: '0_1_2_4', label: 'OPEN SHORTED OR GROUNDED COMPRESSOR MOTOR WINDINGS', trigger: '-1'},
                        { value: '0_1_2_5', label: 'COMPRESSOR STUCK', trigger: '-1'},
                        { value: '0_1_2_6', label: 'COMPRESSOR INTERNAL PROTECTION OPEN', trigger: '-1'},
                        { value: '0_1_2_7', label: 'DEFECTIVE RUN CAPACITOR', trigger: '-1'},
                        { value: '0_1_2_8', label: 'DEFECTIVE START CAPACITOR', trigger: '-1'},
                    ],
                },
                {
                    id: '0_2',
                    options: [
                        { value: '0_2_1', label: 'OUTDOOR FAN STOPPED OR CYCLING ON OVERLOAD', trigger: '0_2_1' },
                        { value: '0_2_2', label: 'OUTDOOR AIR RESTRICTED OR RECIRCULATING', trigger: '-1' },
                        { value: '0_2_3', label: 'RESTRICTED DISCHARGE TUBE', trigger: '-1' },
                        { value: '0_2_4', label: 'OVERCHARGE OR NON-CONDENSABLES IN SYSTEM', trigger: '-1' },
                        { value: '0_2_5', label: 'LOW REFRIGERANT CHARGE', trigger: '-1' },
                        { value: '0_2_6', label: 'LINE VOLTAGE TOO HIGH OR LOW', trigger: '-1' },
                        { value: '0_2_7', label: 'DEFECTIVE RUN CAPACITOR', trigger: '-1' },
                        { value: '0_2_8', label: 'COMPRESSOR BEARINGS', trigger: '-1' },
                        { value: '0_2_9', label: 'HIGH SUPERHEAT', trigger: '-1' },
                        { value: '0_2_10', label: 'DEFECTIVE START CAPACITOR', trigger: '-1' },
                    ],
                },
                {
                    id: '0_2_1',
                    options: [
                        { value: '0_2_1_1', label: 'LOOSE LEAD AT FAN MOTOR', trigger: '-1' },
                        { value: '0_2_1_2', label: 'DEFROST RELAY N.C. CONTACTS OPEN', trigger: '-1' },
                        { value: '0_2_1_3', label: 'MOTOR DEFECTIVE', trigger: '-1' },
                        { value: '0_2_1_4', label: 'INCORRECT OFM CAPACITOR', trigger: '-1' },
                        { value: '0_2_1_5', label: 'DEFECTIVE DEFROST THERMOSTAT', trigger: '-1' },
                    ],
                },
                {
                    id: '0_3',
                    options: [
                        { value: '0_3_1', label: 'LOW SUCTION PRESSURE', trigger: '0_3_1' },
                        { value: '0_3_2', label: 'HIGH SUCTION LOW HEAD PRESSURE', trigger: '0_3_2' },
                        { value: '0_3_3', label: 'HIGH SUCTION LOW SUPERHEAT', trigger: '0_3_3' },
                    ],
                },
                {
                    id: '0_3_1',
                    options: [
                        { value: '0_3_1_1', label: 'DIRTY AIR FILTERS', trigger: '-1' },
                        { value: '0_3_1_2', label: 'DUCT RESTRICTED', trigger: '-1' },
                        { value: '0_3_1_3', label: 'DAMPERS PARTLY CLOSED', trigger: '-1' },
                        { value: '0_3_1_4', label: 'INDOOR COIL FROSTED', trigger: '-1' },
                        { value: '0_3_1_5', label: 'SLIGHTLY LOW ON REFRIGERANT', trigger: '-1' },
                        { value: '0_3_1_6', label: 'LIQUID LINE SLIGHTLY RESTRICTED', trigger: '-1' },
                        { value: '0_3_1_7', label: 'PISTON RESTRICTED', trigger: '-1' },
                        { value: '0_3_1_8', label: 'INCORRECT SIZE PISTON', trigger: '-1' },
                        { value: '0_3_1_9', label: 'INDOOR COIL STRAINER RESTRICTED', trigger: '-1' },
                        { value: '0_3_1_10', label: 'INDOOR BLOWER MOTOR DEFECTIVE OR CYCLING ON OL', trigger: '-1' },
                    ],
                },
                {
                    id: '0_3_2',
                    options: [
                        { value: '0_3_2_1', label: 'REVERSING VALVE HUNG UP OR INTERNAL LEAK', trigger: '-1' },
                        { value: '0_3_2_2', label: 'DEFECTIVE COMPRESSOR VALVES', trigger: '-1' },
                        { value: '0_3_2_3', label: 'INTERNAL PRESSURE RELIEF OPEN', trigger: '-1' },
                    ],
                },
                {
                    id: '0_3_3',
                    options: [
                        { value: '0_3_3_1', label: 'UNIT OVERCHARGED', trigger: '-1' },
                        { value: '0_3_3_2', label: 'INCORRECT SIZE PISTON', trigger: '-1' },
                        { value: '0_3_3_3', label: 'FAILED TXV', trigger: '-1' },
                    ],
                },
            ],
            [
                {
                    id: '-1',
                    message: 'OK. Thanks. Have a nice day',
                    // trigger: '0',
                },
            ],
            [
                {
                    id: '-1',
                    message: 'OK. Thanks. Have a nice day',
                    // trigger: '0',
                },
            ],
            [
                {
                    id: '-1',
                    message: 'OK. Thanks. Have a nice day',
                    // trigger: '0',
                },
            ],
        ];
        return(
            <View 
                style={styles.container}>
                <View style = {styles.header}> 
                    <TouchableOpacity style = {{paddingLeft:width(2.5)}}
                        onPress= {() => this.props.navigation.goBack(null)}>
                        <Image
                            source={images.back_btn}
                            resizeMode='contain'
                            style = {styles.backButton}
                        />
                    </TouchableOpacity>
                   
                    <Text style = {styles.headerFont}> 
                        Fixii Assistance
                    </Text>

                </View>
                
                <View>
                    <ChatBot steps={steps[this.props.navigation.state.params.itemId - 1]} 
                        style = {{height: height(88.5)}} />
                </View>
                

            </View>

            
        )
    }
}

const styles = StyleSheet.create({
    
    // container: {
    //     flex: 1,
        
    // },

    headerFont: {
        paddingLeft: width(27.5),
        color: 'white',
        fontSize: 20,
    },

    backButton: {
        // backgroundColor: 'red',
        width: height(3),
    },

    header:{
        ...Platform.select({
            ios: {
              paddingTop: 24,
              height:height(9.5),
            },
            android: {
                height:height(7.5),
            }
        }),
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#21406e',
        width:width(100),
    },

    bgContainer: {
        flex: 1,
        
    },

   

});
export default ChatBotScreen;