import React, { Component } from 'react';
import { View, Image, StyleSheet, Text, Platform, TextInput,TouchableOpacity } from 'react-native';
import {width,height} from 'react-native-dimension';
import images from '../../const/images';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class ClaimScreen extends Component{
    state = {
        text: ''
      };
    render(){
        return(

            <View 
                style={styles.container}>
                 <View style = {styles.header}>
                    <TouchableOpacity style = {{paddingLeft:width(2.5)}}
                        onPress= {() => this.props.navigation.goBack(null)}>
                        <Image
                            source={images.back_btn}
                            resizeMode='contain'
                            style = {styles.backButton}
                        />
                    </TouchableOpacity>
                   
                    <Text style = {styles.headerFont}> 
                        Warranty Claim
                    </Text>

                </View>

                <KeyboardAwareScrollView  
                        resetScrollToCoords={{x : 0, y: 0}}
                        scrollEnabled={false}
                        // behavior="padding" 
                        enabled>
                    <View style = {styles.textRange}>
                        <Text style = {styles.titleHeader}>
                            Issue :
                        </Text>

                        <TextInput
                            label='Issue'
                            mode = 'flat'
                            placeholder = "Common Issues"
                            underlineColorAndroid={'rgba(0,0,0,0)'}
                            style = {[styles.inputBox, {marginTop:height(2)}]}
                        />
                        <Text style = {[styles.titleHeader, {marginTop: height(3)}]}>
                            Postal Code :
                        </Text>

                        <TextInput
                            label='Postal'
                            mode = 'flat'
                            placeholder = "00000"
                            underlineColorAndroid={'rgba(0,0,0,0)'}
                            style = {[styles.inputBox, {marginTop:height(2)}]}
                        />
                    </View>
                

                <View style = {styles.imageRange}>

                    <View style = {styles.imageText}>
                        <Text style = {styles.imageFont}>
                            Pictures:
                        </Text>
                    </View>

                    <View style = {styles.imageWidget}>

                            <Image
                                source={images.claim_pump}
                                resizeMode='contain'
                                style={{width:width(20)}}
                            />
                        

                        <TouchableOpacity>
                            <Image
                                source={images.add_btn}
                                resizeMode='contain'
                                style={[styles.button,{marginLeft:width(15)}]}
                            />
                        </TouchableOpacity>
                    </View>

                </View>

                <View style = {styles.buttonRange}>
                    <TouchableOpacity
                        onPress= {() => this.props.navigation.goBack(null)}>
                            <Image
                                source={images.smt_btn}
                                resizeMode='contain'
                                style={styles.submitButton}
                            />
                        </TouchableOpacity>
                </View>
                </KeyboardAwareScrollView>
            </View>
           
        )
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        
    },

    headerFont: {
        paddingLeft: width(27.5),
        color: 'white',
        fontSize: 20,
    },

    backButton: {
        // backgroundColor: 'red',
        width: height(3),
    },

    header:{
        ...Platform.select({
            ios: {
              paddingTop: 24,
              height:height(9.5),
            },
            android: {
                height:height(7.5),
            }
        }),
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#21406e',
        width:width(100),
    },

    button:{
        width: width(30),
    },

    submitButton: {
        width: width(90),
        justifyContent: 'center',
        alignItems: 'center',
    },
    
    titleHeader:{
        fontSize:20,
    },

    inputBox: {
        width: width(85),
        height:height(3),
        borderWidth: 1,
        ...Platform.select({
            ios: {
              height: height(7),
              fontSize: 14,

            },
            android: {
                fontSize: 14,
                height: height(7),
            },
        }),        
        paddingHorizontal: 16,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
    
    },
    textRange:{
        flex : 4,
        marginLeft: width(5),
        marginTop: height(5),
        // backgroundColor: 'red',
        justifyContent:'center',
        // alignItems: 'center',
    },

    imageFont:{
        fontSize:20,
    },

    imageText:{
        flex: 1,
        justifyContent: 'center',
        marginLeft: width(5),
        // backgroundColor: 'red',
    },
    imageWidget:{
        flex: 3,
        marginLeft: width(5),
        // backgroundColor: 'blue',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },

    imageRange:{
        flex : 4,
        marginTop:height(5),
    },

    buttonRange:{
        flex : 1,
        // backgroundColor: 'yellow',
        marginTop:height(5),
        justifyContent: 'center',
        alignItems: 'center',
    },

    
});
export default ClaimScreen;