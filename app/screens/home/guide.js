import React, { Component } from 'react';
import { View, Image, StyleSheet, Text, Platform, TouchableOpacity } from 'react-native';
import {width,height} from 'react-native-dimension';
import images from '../../const/images';

class GuideScreen extends Component{
    call = () => {
        this.props.navigation.navigate('Call');
    }
    chatbotscr = () => {
        // alert(this.props.navigation.state.params.itemId);
        this.props.navigation.navigate('ChatBotScr', {
            itemId: this.props.navigation.state.params.itemId,
        });
    }
    
    render(){
        const otherParam = this.props.navigation.getParam('otherParam');
        const paramImage = this.props.navigation.getParam("image");
        
        return(
            <View 
                style={styles.container}>
                <View style = {styles.header}>
                    <TouchableOpacity style = {{paddingLeft:width(2.5)}}
                        onPress= {() => this.props.navigation.goBack(null)}>
                        <Image
                            source={images.back_btn}
                            resizeMode='contain'
                            style = {styles.backButton}
                        />
                    </TouchableOpacity>
                   
                    <Text style = {styles.headerFont}> 
                        {JSON.parse(JSON.stringify(otherParam))}
                    </Text>

                </View>

                <View style={styles.bgContainer}>
                    <View style = {styles.imgRange}>
                        <Image  
                            source= {paramImage}
                            resizeMode='contain'
                            style={styles.image}>

                        </Image>
                    </View>
                    
                    <View style = {styles.descriptionRange}>
                        <View style = {styles.titleWidget}>
                            <Text style = {styles.titleText}>
                                {JSON.parse(JSON.stringify(otherParam))}
                            </Text>
                        </View>

                        <View style = {styles.bodyWidget}>
                            <Text style = {styles.bodyText}>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </Text>
                        </View>
                    </View>

                    <View style = {styles.buttonRange}>
                        <View style = {styles.buttonWidget}>
                            <TouchableOpacity onPress={this.chatbotscr}>
                                <Image
                                    source={images.chat_btn}
                                    resizeMode='contain'
                                    style={styles.button}
                                />
                            </TouchableOpacity>
                        </View>
                        
                        <View style = {[styles.buttonWidget, {'marginLeft':width(5)}]}>
                            <TouchableOpacity onPress={this.call}>
                                <Image
                                    source={images.assist_btn}
                                    resizeMode='contain'
                                    style={styles.button}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        
    },

    headerFont: {
        paddingLeft: width(27.5),
        color: 'white',
        fontSize: 20,
    },

    backButton: {
        // backgroundColor: 'red',
        width: height(3),
    },

    header:{
        ...Platform.select({
            ios: {
              paddingTop: 24,
              height:height(9.5),
            },
            android: {
                height:height(7.5),
            }
        }),
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#21406e',
        width:width(100),
    },

    bgContainer: {
        flex: 1,
        
    },

    imgRange: {
        flex:0.3,
        justifyContent: 'center',
        alignItems: 'center',
    },

    descriptionRange: {
        flex:0.6,
        alignItems: 'center',
        justifyContent:'center',
    },
    
    titleWidget:{
        flex: 1,
        width: width(90),
        // alignItems: 'center',
        justifyContent:'center',
    },

    titleText:{
        alignItems: 'center',
        justifyContent:'center',
        fontSize: 20,
        color: 'black',
    },
    
    bodyWidget:{
        flex: 3,
        width: width(90),
        justifyContent:'center',

    },

    bodyText:{
        
        fontSize: 15,
        color: 'black',
    },

    image:{
        justifyContent: 'center',
        alignItems: 'center',
        height: height(15),
    },
    
    buttonWidget:{
        // justifyContent: 'center',
        // alignItems: 'center',
    },

    button:{
        // backgroundColor:'green',
        // justifyContent: 'center',
        // alignItems: 'center',
         width:width(45),
    },

    buttonRange: {
        flex:0.1,
        
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

});
export default GuideScreen;