import React, { Component } from 'react';
import { View, Image,ImageBackground, StyleSheet, Text, Platform, TouchableOpacity } from 'react-native';
import {width,height} from 'react-native-dimension';
import images from '../../const/images';

class HomeScreen extends Component{
    

    assist = () => {
        this.props.navigation.navigate('Assist');
    }

    claim = () => {
        this.props.navigation.navigate('Claim');
    }
    
    render(){
        return(
            <View 
                style={styles.container}>
                 <View style = {styles.header}>
                    <Text style = {styles.headerFont}> 
                        Fixii Pro
                    </Text>
                </View>
                <View 
                    style={styles.topContainer}>
                   
                    <Image 
                        source={images.logo}
                        style={styles.logo}
                        resizeMode='contain'
                    />
              
                </View>

                <View
                    style={styles.bottomContainer}>
                        <TouchableOpacity onPress={this.assist}>
                            <Image
                                source={images.diag_btn}
                                resizeMode='contain'
                                style={styles.button}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.claim}>
                            <Image
                                source={images.warranty_btn}
                                resizeMode='contain'
                                style={styles.button}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.claim}>
                            <Image
                                source={images.document_btn}
                                resizeMode='contain'
                                style={styles.button}
                            />
                        </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
    },

    topContainer: {
        flex: 0.5,
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
    },

    headerFont: {
        
        color: 'white',
        fontSize: 20,
    },

    header:{
        ...Platform.select({
            ios: {
              paddingTop: 24,
              height:height(9.5),
            },
            android: {
                height:height(7.5),
            }
        }),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#21406e',
        width:width(100),
    },

    logo: {
        justifyContent: 'center',
        alignItems: 'center',
        width: width(100),
        height: height(30),
    },

    button: {
        marginTop: height(1),
        // backgroundColor:"white",
        justifyContent: 'center',
        alignItems: 'center',
        width: width(90),
        height: height(12.5),
    },

    bottomContainer: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'blue',
    }
    
    
});
export default HomeScreen;